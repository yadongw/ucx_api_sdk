<?php
/**
 * Created by PhpStorm.
 * User: yadongw
 * Date: 15-5-21
 * Time: 下午4:49
 */
namespace UcxApiSdk;

use UcxApiSdk\Resource\Auth;
use UcxApiSdk\Resource\BaseResource;
use UcxApiSdk\Resource\Document;
use UcxApiSdk\Resource\Library;

class Client
{
	public function setAccessToken ($accessToken)
	{
		BaseResource::$access_token = $accessToken;
	}

	public function getDocument ()
	{
		return new Document();
	}

	public function getLibrary ()
	{
		return new Library();
	}

	public function getAuth ()
	{
		return new Auth();
	}
}