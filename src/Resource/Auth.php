<?php
/**
 * Created by PhpStorm.
 * User: yadongw
 * Date: 15-5-26
 * Time: 下午2:21
 */
namespace UcxApiSdk\Resource;
use GuzzleHttp\Exception\ServerException;
class Auth extends BaseResource
{
	static $url = '/auth';

	public function signIn($email, $password)
	{
		try{
			$response = $this->client->post($this->apiUrl('access-token'), ['json' => ['email' => $email, 'password' => $password]]);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}

	public function signUp($email, $password)
	{
		try{
			$response = $this->client->post($this->apiUrl('signup'), ['json' => ['email' => $email, 'password' => $password]]);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}

	public function changePassword($oldPassword, $newPassword)
	{
		$response = $this->client->post($this->apiUrl('change-password'), ['json' => ['query' => $email, 'password' => $password]]);
		return $this->handleResponse($response);
	}

	public function getCurrentInfo()
	{
		$response = $this->client->post($this->apiUrl('get-user-info'));
		return $this->handleResponse($response);
	}

	public function updateCurrentInfo($data)
	{
		$response = $this->client->post($this->apiUrl('update-user-info'), ['json' => $data]);
		return $this->handleResponse($response);
	}

	public function create($data){}

	public function update($id, $data){}

	public function delete($id){}

	public function find($id = null, $expand = ''){}

	public function findAll($page = 1, $expand = ''){}
}