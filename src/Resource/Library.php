<?php
/**
 * Created by PhpStorm.
 * User: yadongw
 * Date: 15-5-26
 * Time: 下午2:21
 */
namespace UcxApiSdk\Resource;

use GuzzleHttp\Exception\ServerException;

class Library extends BaseResource
{
	static $url = '/library';

	public function getDocuments($lid, $page = 1, $expand = '')
	{
		$url = $this->apiUrl($lid. '/documents', ['page' => $page, 'expand' => $expand]);

		try{
			$response = $this->client->get($url);
		}catch (ServerException $e){
			return $this->handleResponse($e->getR);
		}
		return $this->handleResponse($response);
	}
}