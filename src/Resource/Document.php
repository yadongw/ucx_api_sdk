<?php
/**
 * Created by PhpStorm.
 * User: yadongw
 * Date: 15-5-26
 * Time: 下午2:21
 */
namespace UcxApiSdk\Resource;

use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Stream\Stream;

class Document extends BaseResource
{
	static $url = '/document';

	public function upload($file, $name='', $description='', $lid= '' )
	{
		$resource = fopen($file, 'r');
		$url = $this->apiUrl();
		try{
			$response = $this->client->post($url,
			                                [
				                                'body' => ['pdf_file' => $resource,'name' => $name, 'description' => $description, 'lid' => $lid],
				                                'headers' => ['Content-Type' => 'application/octet-stream']

			                                ]);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);

	}

	public function download($id)
	{
		return $this->apiUrl($id . '/download');
	}

	public function preview($id)
	{
		$url = $this->apiUrl($id . '/preview');
		try{
			$response = $this->client->get($url);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}

	public function webLink ($id)
	{
		$url = $this->apiUrl($id . '/share-web-link');
		try{
			$response = $this->client->post($url);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}

	public function secureSend ($id)
	{
		$url = $this->apiUrl($id . '/share-secure-send');
		try{
			$response = $this->client->post($url, ['json' => $data]);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}

	public function getShare ($shareId)
	{
		$url = $this->apiUrl('doc_id' . '/share/' . $shareId);
		try{
			$response = $this->client->get($url);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}

	public function getShares ($id)
	{
		$url = $this->apiUrl($id . '/shares');
		try{
			$response = $this->client->get($url);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}

	public function deleteShare ($shareId)
	{
		$url = $this->apiUrl('doc_id' . '/share/' . $shareId);
		try{
			$response = $this->client->delete($url);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}

		return $this->handleResponse($response);
	}

	public function deleteAllShare ($id)
	{
		$url = $this->apiUrl($id . '/shares');
		try{
			$response = $this->client->delete($url);
		}catch (ServerException $e){
			$this->handleResponse($e->getResponse());
		}
		return $this->handleResponse($response);
	}
}